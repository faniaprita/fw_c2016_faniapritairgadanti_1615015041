@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Data pembeli
		<div class="pull-right">
			<a href="{{ url('pembeli/tambah')}}" class="btn btn-primary btn-xs"></img>Tambah Data</a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
				<tr>
					<td>Nama</td>
					<td>No Telepon</td>
					<td>Email</td>
					<td>Alamat</td>
					<td>Pengguna_Id</td>
					<td>Aksi</td>
				</tr>
				@foreach($pembeli as $pembeli)
					
				<tr>
					<td >{{ $pembeli->nama}}</td>
					<td >{{ $pembeli->notlp}}</td>
					<td >{{ $pembeli->email}}</td>
					<td >{{ $pembeli->alamat}}</td>
					<td >{{ $pembeli->pengguna_id}}</td>
					<td >
					
					<a href="{{url('pembeli/edit/'.$pembeli->id)}}" class="btn btn-success btn-xs">Edit</a>
					<a href="{{url('pembeli/hapus/'.$pembeli->id)}}" class="btn btn-danger btn-xs">Hapus</a>
					</td>
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection