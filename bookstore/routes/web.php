<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('hello-world',function(){
return 'hello World';
});

Route::get('pengguna/{pengguna}',function ($pengguna)
{
return "Hallo World dari pengguna $pengguna";
});

Route::get('berita/{berita?}',function ($berita= "Laravel 5")
{
return "Berita $berita belum dibaca";
});

Route::get('/test','penggunacontroller@awal');
Route::get('/tambah','penggunacontroller@tambah');

Route::get('/buku/awal',"BukuController@awal");
Route::get('/buku/lihat',"BukuController@lihat");
Route::get('/buku/edit/{buku}',"BukuController@edit");
Route::post('/buku/update/{buku}',"BukuController@update");
Route::get('/buku/hapus/{buku}',"BukuController@hapus");

Route::get('/penulis/tambah',"PenulisController@tambah");
Route::post('/penulis/simpan',"PenulisController@simpan");
Route::get('/penulis',"PenulisController@awal");
Route::get('/penulis/edit/{penulis}',"PenulisController@edit");
Route::post('/penulis/update/{penulis}',"PenulisController@update");
Route::get('/penulis/hapus/{penulis}',"PenulisController@hapus");

Route::get('/buku/tambah',"BukuController@tambah");
Route::post('/buku/simpan',"BukuController@simpan");

Route::get('/kategori/tambah',"KategoriController@tambah");
Route::post('/kategori/simpan',"KategoriController@simpan");
Route::get('/kategori/edit/{kategori}',"KategoriController@edit");
Route::post('/kategori/update/{kategori}',"KategoriController@update");
Route::get('/kategori/hapus/{kategori}',"KategoriController@hapus");

Route::get('/pembeli/tambah',"PembeliController@tambah");
Route::post('/pembeli/simpan',"PembeliController@simpan");
Route::get('/pembeli',"PembeliController@awal");
Route::get('/pembeli/edit/{pembeli}',"PembeliController@edit");
Route::post('/pembeli/update/{pembeli}',"PembeliController@update");
Route::get('/pembeli/hapus/{pembeli}',"PembeliController@hapus");

Route::get('/admin',"AdminController@awal");
Route::get('/admin/tambah',"AdminController@tambah");
Route::post('/admin/simpan',"AdminController@simpan");
Route::get('/admin/lihat',"AdminController@lihat");
Route::get('/admin/edit/{admin}',"AdminController@edit");
Route::post('/admin/update/{admin}',"AdminController@update");
Route::get('/admin/hapus/{admin}',"AdminController@hapus");



