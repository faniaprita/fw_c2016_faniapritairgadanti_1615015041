<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\buku;

class kategori extends Model
{
     protected $table='kategori';
     protected $fillable=['deskripsi'];

     public function buku(){
	 return $this->hasMany('App\buku');
	}

}
