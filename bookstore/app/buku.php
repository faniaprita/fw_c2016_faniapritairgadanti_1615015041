<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\kategori;
use App\penulis;

class buku extends Model
{
    protected $table='buku';
    protected $fillable=['judul','kategori_id','penerbit','tanggal'];

    public function kategori(){
		return $this->belongsTo('App\kategori');
	}
	
	public function penulis(){
		return $this->belongsToMany(penulis::class)->withPivot('id');
	}

	public function pembeli(){
		return $this->belongsToMany(pembeli::class);
	}

}
